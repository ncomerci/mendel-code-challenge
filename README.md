# Mendel Code Challenge

This project is a RESTful web service for storing and retrieving transaction information. It is implemented using Spring Boot and Java 21. The service allows you to store transactions, retrieve transactions by type, and calculate the total amount involved for all transactions linked to a particular transaction.

## Features

- **Store Transactions**: Add transactions with a specified type and amount.
- **Retrieve Transactions by Type**: Get all transactions of a specified type.
- **Calculate Transaction Sum**: Calculate the sum of all transactions linked to a specific transaction.

## Running the Application with Docker

To run the application using Docker, follow these steps:

1. **Build the Docker Image**:

   Open a terminal and navigate to the project root directory. Run the following command to build the Docker image:

   ```bash
   docker build -t mendel-code-challenge .
   ```

2. **Run the Docker Container**:

   After building the image, run the following command to start the application in a Docker container:

   ```bash
   docker run -p 8080:8080 mendel-code-challenge
   ```

   This command maps port 8080 of the container to port 8080 of your host machine.

3. **Access the Application**:

   Once the container is running, you can access the application at `http://localhost:8080`.

## API Endpoints

- **PUT /transactions/{transaction_id}**: Add a new transaction.
    - Request Body: `{ "amount": double, "type": string, "parent_id": long (optional) }`

- **GET /transactions/types/{type}**: Get all transaction IDs of a specified type.

- **GET /transactions/sum/{transaction_id}**: Get the sum of all transactions linked to a specified transaction.

## Example

1. **Add a Transaction**:
   ```bash
   curl -X PUT -H "Content-Type: application/json" -d '{"amount":1000.0,"type":"payment"}' http://localhost:8080/transactions/1
   ```

2. **Retrieve Transactions by Type**:
   ```bash
   curl http://localhost:8080/transactions/types/payment
   ```

3. **Calculate Transaction Sum**:
   ```bash
   curl http://localhost:8080/transactions/sum/1
   ```