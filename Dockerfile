# Stage 1: Build the application
FROM openjdk:21-jdk-slim AS build

# Set the working directory inside the container
WORKDIR /app

# Copy the Maven wrapper and the Maven configuration directory
COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .

# Grant execution permissions to the Maven wrapper script
RUN chmod +x mvnw

# Download the project dependencies without running tests
RUN ./mvnw dependency:go-offline -B

# Copy the source code to the working directory
COPY src src

# Build the project, generating the JAR file
RUN ./mvnw clean package

# Stage 2: Create the runtime image
FROM openjdk:21-jdk-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the built JAR file from the build stage
COPY --from=build /app/target/code_challenge-0.0.1-SNAPSHOT.jar app.jar

# Expose the port the application will run on
EXPOSE 8080

# Command to run the application
ENTRYPOINT ["java", "-jar", "app.jar"]
