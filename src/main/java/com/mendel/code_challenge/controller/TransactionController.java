package com.mendel.code_challenge.controller;

import com.mendel.code_challenge.model.Transaction;
import com.mendel.code_challenge.model.TransactionRequest;
import com.mendel.code_challenge.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * REST controller for managing transactions.
 */
@RestController
@RequestMapping("/transactions")
public class TransactionController {
    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Adds a new transaction.
     *
     * @param transactionId The unique identifier of the transaction.
     * @param transactionRequest The transaction request containing amount, type, and parentId.
     * @return A ResponseEntity indicating the result of the operation.
     */
    @PutMapping("/{transaction_id}")
    public ResponseEntity<Void> addTransaction(@PathVariable("transaction_id") long transactionId,
                                               @RequestBody TransactionRequest transactionRequest) {
        Transaction newTransaction = new Transaction(transactionId, transactionRequest);
        transactionService.addTransaction(newTransaction);
        return ResponseEntity.ok().build();
    }

    /**
     * Retrieves transaction IDs by type.
     *
     * @param type The type of the transactions to retrieve.
     * @return A ResponseEntity containing a list of transaction IDs matching the specified type.
     */
    @GetMapping("/types/{type}")
    public ResponseEntity<List<Long>> getTransactionIdsByType(@PathVariable("type") String type) {
        List<Long> transactionIds = transactionService.getTransactionIdsByType(type);
        return ResponseEntity.ok(transactionIds);
    }

    /**
     * Calculates the sum of all transactions linked to a specific transaction.
     *
     * @param transactionId The unique identifier of the root transaction.
     * @return A ResponseEntity containing the sum of the amounts of the root transaction and all linked transactions.
     */
    @GetMapping("/sum/{transaction_id}")
    public ResponseEntity<Map<String, Double>> getTransactionSum(@PathVariable("transaction_id") long transactionId) {
        double sum = transactionService.getTransactionSum(transactionId);
        return ResponseEntity.ok(Map.of("sum", sum));
    }
}
