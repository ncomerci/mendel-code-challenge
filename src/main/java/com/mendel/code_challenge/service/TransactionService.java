package com.mendel.code_challenge.service;

import com.mendel.code_challenge.model.Transaction;

import java.util.List;

/**
 * Service interface for managing transactions.
 */
public interface TransactionService {

    /**
     * Retrieves a transaction by its unique identifier.
     *
     * @param transactionId The unique identifier of the transaction.
     * @return The transaction with the specified ID.
     */
    Transaction getTransactionById(long transactionId);

    /**
     * Adds a new transaction.
     *
     * @param transaction The transaction to add.
     */
    void addTransaction(Transaction transaction);

    /**
     * Retrieves a list of transaction IDs by type.
     *
     * @param type The type of the transactions to retrieve.
     * @return A list of transaction IDs matching the specified type.
     */
    List<Long> getTransactionIdsByType(String type);

    /**
     * Calculates the sum of all transactions linked to a specific transaction.
     *
     * @param transactionId The unique identifier of the root transaction.
     * @return The sum of the amounts of the root transaction and all linked transactions.
     */
    double getTransactionSum(long transactionId);
}
