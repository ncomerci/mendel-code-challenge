package com.mendel.code_challenge.service;

import com.mendel.code_challenge.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final Map<Long, Transaction> transactionMap;

    public TransactionServiceImpl() {
        this.transactionMap = new HashMap<>();
    }

    @Override
    public Transaction getTransactionById(long transactionId) {
        return transactionMap.get(transactionId);
    }

    @Override
    public void addTransaction(Transaction transaction) {
        transactionMap.put(transaction.getId(), transaction);
    }

    @Override
    public List<Long> getTransactionIdsByType(String type) {
        return transactionMap.values().stream()
                .filter(t -> t.getType().equals(type))
                .map(Transaction::getId)
                .collect(Collectors.toList());
    }

    @Override
    public double getTransactionSum(long transactionId) {
        Transaction transaction = transactionMap.get(transactionId);
        return transaction != null ? calculateTransactionSum(transaction) : 0;
    }

    private double calculateTransactionSum(Transaction transaction) {
        double sum = 0;
        Queue<Transaction> queue = new LinkedList<>();
        queue.add(transaction);

        while (!queue.isEmpty()) {
            Transaction current = queue.poll();
            sum += current.getAmount();

            List<Transaction> childTransactions = transactionMap.values().stream()
                    .filter(t -> Objects.equals(current.getId(), t.getParentId()))
                    .toList();

            queue.addAll(childTransactions);
        }

        return sum;
    }
}
