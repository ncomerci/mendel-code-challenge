package com.mendel.code_challenge.model;

/**
 * Represents a request to create a transaction.
 */
public class TransactionRequest {
    private final double amount;
    private final String type;
    private final Long parentId;

    /**
     * Constructs a new TransactionRequest.
     *
     * @param amount The amount of the transaction. Must be non-negative.
     * @param type The type of the transaction. Cannot be null.
     * @param parentId The ID of the parent transaction, if any. Can be null.
     */
    public TransactionRequest(double amount, String type, Long parentId) {
        if (amount < 0) {
            throw new IllegalArgumentException("amount must be non-negative");
        }
        if (type == null) {
            throw new NullPointerException("type cannot be null");
        }
        this.amount = amount;
        this.type = type;
        this.parentId = parentId;
    }

    /**
     * @return The amount of the transaction.
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @return The type of the transaction.
     */
    public String getType() {
        return type;
    }

    /**
     * @return The parent ID of the transaction, or null if there is no parent.
     */
    public Long getParentId() {
        return parentId;
    }
}
