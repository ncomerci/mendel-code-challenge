package com.mendel.code_challenge.model;

import java.util.Objects;

/**
 * Represents a transaction, extending the attributes of TransactionRequest.
 */
public class Transaction extends TransactionRequest {
    private final long id;

    /**
     * Constructs a new Transaction.
     *
     * @param id The unique identifier of the transaction.
     * @param transactionRequest The transaction request containing amount, type, and parentId.
     */
    public Transaction(long id, TransactionRequest transactionRequest) {
        super(transactionRequest.getAmount(), transactionRequest.getType(), transactionRequest.getParentId());
        this.id = id;
    }

    /**
     * @return The unique identifier of the transaction.
     */
    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction that)) return false;
        return getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
