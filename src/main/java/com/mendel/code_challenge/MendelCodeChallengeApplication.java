package com.mendel.code_challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MendelCodeChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MendelCodeChallengeApplication.class, args);
    }
}
