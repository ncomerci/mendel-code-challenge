package com.mendel.code_challenge.service;

import com.mendel.code_challenge.model.Transaction;
import com.mendel.code_challenge.model.TransactionRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionServiceImplTest {

    private TransactionService transactionService;

    @BeforeEach
    public void setUp() {
        transactionService = new TransactionServiceImpl();
    }

    @Test
    public void testAddTransaction() {
        // Arrange
        TransactionRequest transactionRequest = new TransactionRequest(100, "payment", null);
        Transaction transaction = new Transaction(1, transactionRequest);

        // Act
        transactionService.addTransaction(transaction);

        // Assert
        assertEquals(transaction.getAmount(), transactionService.getTransactionSum(1));
    }

    @Test
    public void testGetTransactionIdsByType() {
        // Arrange
        TransactionRequest transactionRequest1 = new TransactionRequest(100, "payment", null);
        TransactionRequest transactionRequest2 = new TransactionRequest(200, "refund", null);
        TransactionRequest transactionRequest3 = new TransactionRequest(300, "payment", null);
        Transaction transaction1 = new Transaction(1, transactionRequest1);
        Transaction transaction2 = new Transaction(2, transactionRequest2);
        Transaction transaction3 = new Transaction(3, transactionRequest3);
        transactionService.addTransaction(transaction1);
        transactionService.addTransaction(transaction2);
        transactionService.addTransaction(transaction3);

        // Act
        List<Long> paymentIds = transactionService.getTransactionIdsByType("payment");
        List<Long> refundIds = transactionService.getTransactionIdsByType("refund");

        // Assert
        assertEquals(2, paymentIds.size());
        assertEquals(1, paymentIds.getFirst());
        assertEquals(3, paymentIds.get(1));
        assertEquals(1, refundIds.size());
        assertEquals(2, refundIds.getFirst());
    }

    @Test
    public void testGetTransactionSum() {
        // Arrange
        TransactionRequest transactionRequest1 = new TransactionRequest(100, "payment", null);
        TransactionRequest transactionRequest2 = new TransactionRequest(200, "refund", 1L);
        TransactionRequest transactionRequest3 = new TransactionRequest(50, "payment", null);
        Transaction transaction1 = new Transaction(1, transactionRequest1);
        Transaction transaction2 = new Transaction(2, transactionRequest2); // Refund for transaction1
        Transaction transaction3 = new Transaction(3, transactionRequest3);
        transactionService.addTransaction(transaction1);
        transactionService.addTransaction(transaction2);
        transactionService.addTransaction(transaction3);

        // Act
        double sum1 = transactionService.getTransactionSum(1); // Sum for transaction1
        double sum2 = transactionService.getTransactionSum(2); // Sum for transaction2
        double sum3 = transactionService.getTransactionSum(3); // Sum for transaction3

        // Assert
        assertEquals(300, sum1);
        assertEquals(200, sum2);
        assertEquals(50, sum3);
    }
}
