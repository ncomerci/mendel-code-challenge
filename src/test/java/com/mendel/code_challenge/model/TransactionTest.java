package com.mendel.code_challenge.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionTest {
    @Test
    void testValidTransaction() {
        // Arrange
        long id = 1;
        double amount = 100;
        String type = "payment";
        Long parentId = null;

        // Act
        TransactionRequest transactionRequest = new TransactionRequest(amount, type, parentId);
        Transaction transaction = new Transaction(id, transactionRequest);

        // Assert
        assertNotNull(transaction);
        assertEquals(id, transaction.getId());
        assertEquals(amount, transaction.getAmount());
        assertEquals(type, transaction.getType());
        assertNull(transaction.getParentId());
    }

    @Test
    void testNegativeAmount() {
        // Arrange
        double amount = -100;
        String type = "payment";
        Long parentId = null;

        // Act & Assert
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> new TransactionRequest(amount, type, parentId));

        // Assert
        assertEquals("amount must be non-negative", exception.getMessage());
    }

    @Test
    void testNullType() {
        // Arrange
        double amount = 100;
        String type = null;
        Long parentId = null;

        // Act & Assert
        NullPointerException exception = assertThrows(NullPointerException.class, () -> new TransactionRequest(amount, type, parentId));

        // Assert
        assertEquals("type cannot be null", exception.getMessage());
    }

    @Test
    public void testEquals() {
        // Arrange
        long id1 = 2;
        double amount1 = 100;
        String type1 = "payment";
        Long parentId1 = 0L;

        long id2 = 3;
        double amount2 = 200;
        String type2 = "refund";
        Long parentId2 = 1L;

        // Act
        TransactionRequest transactionRequest1 = new TransactionRequest(amount1, type1, parentId1);
        TransactionRequest transactionRequest2 = new TransactionRequest(amount2, type2, parentId2);
        Transaction transaction1 = new Transaction(id1, transactionRequest1);
        Transaction transaction2 = new Transaction(id1, transactionRequest1); // Same attributes as transaction1
        Transaction transaction3 = new Transaction(id2, transactionRequest2);

        // Assert
        assertEquals(transaction1, transaction2); // Same attributes, so should be equal
        assertNotEquals(transaction1, transaction3); // Different attributes, so should not be equal
    }
}
