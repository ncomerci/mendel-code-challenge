package com.mendel.code_challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mendel.code_challenge.model.Transaction;
import com.mendel.code_challenge.model.TransactionRequest;
import com.mendel.code_challenge.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private TransactionService transactionService;

    @Test
    @DirtiesContext
    public void testAddTransaction() throws Exception {
        // Arrange
        TransactionRequest transactionRequest = new TransactionRequest(1000.0, "cars", null);

        // Act & Assert
        mockMvc.perform(put("/transactions/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionRequest)))
                .andExpect(status().isOk());

        // Assert
        Transaction transaction = transactionService.getTransactionById(2L);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getAmount()).isEqualTo(1000.0);
        assertThat(transaction.getType()).isEqualTo("cars");
        assertThat(transaction.getParentId()).isNull();
    }

    @Test
    @DirtiesContext
    public void testAddTransactionWithParent() throws Exception {
        // Arrange
        TransactionRequest transactionRequest = new TransactionRequest(200.0, "shopping", 1L);

        // Act & Assert
        mockMvc.perform(put("/transactions/3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(transactionRequest)))
                .andExpect(status().isOk());

        // Assert
        Transaction transaction = transactionService.getTransactionById(3L);
        assertThat(transaction).isNotNull();
        assertThat(transaction.getAmount()).isEqualTo(200.0);
        assertThat(transaction.getType()).isEqualTo("shopping");
        assertThat(transaction.getParentId()).isEqualTo(1L);
    }

    @Test
    @DirtiesContext
    public void testGetTransactionsByType() throws Exception {
        // Arrange
        transactionService.addTransaction(new Transaction(2L, new TransactionRequest(1000.0, "cars", null)));
        transactionService.addTransaction(new Transaction(3L, new TransactionRequest(200.0, "cars", 1L)));
        transactionService.addTransaction(new Transaction(4L, new TransactionRequest(300.0, "shopping", null)));

        // Act & Assert
        mockMvc.perform(get("/transactions/types/cars"))
                .andExpect(status().isOk())
                .andExpect(content().json("[2, 3]"));
    }

    @Test
    @DirtiesContext
    public void testGetTransactionSum() throws Exception {
        // Arrange
        transactionService.addTransaction(new Transaction(2L, new TransactionRequest(1000.0, "cars", null)));
        transactionService.addTransaction(new Transaction(3L, new TransactionRequest(200.0, "cars", 2L)));
        transactionService.addTransaction(new Transaction(4L, new TransactionRequest(300.0, "cars", 3L)));

        // Act & Assert
        mockMvc.perform(get("/transactions/sum/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(1500.0));
    }

    @Test
    @DirtiesContext
    public void testGetTransactionSumWithNoChildren() throws Exception {
        // Arrange
        transactionService.addTransaction(new Transaction(2L, new TransactionRequest(1000.0, "cars", null)));

        // Act & Assert
        mockMvc.perform(get("/transactions/sum/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.sum").value(1000.0));
    }

    @Test
    @DirtiesContext
    public void testAddTransactionWithInvalidAmount() throws Exception {
        // Arrange
        String invalidTransactionJson = """
            {
                "amount": -1000,
                "type": "cars",
                "parentId": null
            }
        """;

        // Act & Assert
        mockMvc.perform(put("/transactions/5")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(invalidTransactionJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    @DirtiesContext
    public void testAddTransactionWithNullType() throws Exception {
        // Arrange
        String invalidTransactionJson = """
            {
                "amount": 1000,
                "type": null,
                "parentId": null
            }
        """;

        // Act & Assert
        mockMvc.perform(put("/transactions/6")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(invalidTransactionJson))
                .andExpect(status().isBadRequest());
    }
}
